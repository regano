#!/usr/bin/env perl

=head1 NAME

regano-extract-zones.pl - Regano DNS zone extraction tool

=head1 SYNOPSIS

regano-extract-zones.pl <base URI> <server name> <output directory>

=head1 DESCRIPTION

Retreive all zones from a Regano instance that list the indicated server
as their master nameserver.  The zone files are placed in a "zones"
directory under the stated output directory.  A BIND configuration file
fragment named "regano-zones.conf" is also placed in the output directory.

=head1 AUTHOR

Pathore

=head1 LICENSE

This program is free software. You can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

use strict;
use warnings;

use LWP 5.64;
use HTTP::Date;
use File::Spec;

my $UA = new LWP::UserAgent;

die "need 3 arguments" unless scalar @ARGV == 3;

my ($baseURI, $master, $outdir) = @ARGV;

$baseURI =~ s!/$!!;
$outdir = File::Spec->rel2abs($outdir);

my $response = $UA->get($baseURI.'/domain/export_list?master='.$master);
die "could not retrieve zone list from $baseURI" unless $response->is_success;

mkdir File::Spec->catdir($outdir, 'zones')
    or die "mkdir: $!"
    unless -d File::Spec->catdir($outdir, 'zones');

my $confname = File::Spec->catfile($outdir, 'regano-zones.conf');
rename $confname, File::Spec->catfile($outdir, 'regano-zones.conf~')
    if -e $confname;
open CONF, '>', $confname or die "$confname: $!";

my @zones = grep {$_} split(' ', $response->decoded_content);
foreach my $zone_name (@zones) {
    my $filename = File::Spec->catfile($outdir, 'zones', $zone_name);
    my @reqopt = ('Accept' => 'text/dns');

    if (-e $filename) {
	push @reqopt, 'If-Modified-Since' => time2str((stat _)[9]);
    }

    my $response = $UA->get($baseURI.'/zone/'.$zone_name, @reqopt);
    die "could not get zone $zone_name"
	unless $response->is_success or $response->code == 304;

    unless ($response->code == 304) {
	open ZONE, '>', $filename
	    or die "$filename: $!";
	print ZONE $response->decoded_content
	    or die "$filename: $!";
	close ZONE or die "$filename: $!";
	utime time, str2time($response->header('Last-Modified')), $filename;
    }

    print CONF <<"EOF";
zone \"$zone_name\" {
    type master;
    file \"$filename\";
}
EOF
}

close CONF or die "$confname: $!";

1;
