#!/usr/bin/perl

use Test::More tests => 2 + 3 + 3;

use DBI;
use strict;
use warnings;

my $dbh = DBI->connect('dbi:Pg:db=regano', undef, undef,
		       {AutoCommit => 1, RaiseError => 1})
  or BAIL_OUT $DBI::errstr;

##

my ($TRUE, $FALSE) = $dbh->selectrow_array(q{SELECT TRUE, FALSE});

my %SESSIONS;
{
  my $sth = $dbh->prepare
    (q{WITH open_sessions AS
	 (SELECT s.*, dense_rank() OVER (PARTITION BY s.user_id
					 ORDER BY s.activity DESC)
	      FROM regano.sessions AS s)
	SELECT s.id, u.username, regano_api.session_check(s.id)
	    FROM open_sessions AS s JOIN regano.users AS u
		ON s.user_id = u.id
	    WHERE dense_rank = 1});
  $sth->execute;
  my ($id, $username, $check);
  $sth->bind_columns(\($id, $username, $check));
  while ($sth->fetch) {
    $SESSIONS{$username} = $id if $check;
  }
}

BAIL_OUT('No sessions in DB') unless scalar keys %SESSIONS;

##

# clean up after a previous test run
$dbh->do(q{TRUNCATE regano.antiskid_block});
$dbh->do(q{TRUNCATE regano.antiskid_tally});

my $act_st = $dbh->prepare(q{SELECT regano_api.antiskid_check(?, ?)});
my $achk_st = $dbh->prepare(q{SELECT regano_api.antiskid_session_check(?, ?)});
my $schk_st = $dbh->prepare(q{SELECT regano_api.session_check(?)});

sub act  ($$) {$dbh->selectrow_array($act_st,  {}, @_)}
sub achk ($$) {$dbh->selectrow_array($achk_st, {}, @_)}
sub schk ($)  {$dbh->selectrow_array($schk_st, {}, @_)}

my $good_addr = '65.110.111.78';
my $evil_addr = '69.118.49.108';

ok(achk($SESSIONS{test1}, $good_addr),
   q{Good client as 'test1' before actions});
ok(achk($SESSIONS{test2}, $evil_addr),
   q{Evil client as 'test2' before actions});

ok(act(q{newuser}, $good_addr),
   q{Good client makes user account});
ok(act(q{newuser}, $evil_addr),
   q{Evil client makes user account});
ok(!act(q{newuser}, $evil_addr),
   q{Evil client attempts to make second user account});

ok(achk($SESSIONS{test1}, $good_addr),
   q{Good client as 'test1' after actions});
ok(!achk($SESSIONS{test2}, $evil_addr),
   q{Evil client blocked after actions});
ok(schk($SESSIONS{test2}) && !achk($SESSIONS{test1}, $evil_addr),
   q{Evil client still blocked with stolen session});

##

$dbh->disconnect;

__END__
