#!/usr/bin/perl

use Test::More tests => 3 + 2 + 5 + 2;

use DBI;
use strict;
use warnings;

use Crypt::Random qw(makerandom_octet);
use Digest::SHA qw(hmac_sha384_base64);
use MIME::Base64 qw(encode_base64);

my $dbh = DBI->connect('dbi:Pg:db=regano', undef, undef,
		       {AutoCommit => 1, RaiseError => 1})
  or BAIL_OUT $DBI::errstr;

##

{
  my $sth = $dbh->prepare(q{INSERT INTO regano.bailiwicks (domain_tail) VALUES (?)});
  $sth->{PrintError} = 0;
  foreach my $bogus_item ('test', '.test', 'test.') {
    $dbh->begin_work;
    eval { $sth->execute($bogus_item) };
    like($@, qr/violates check constraint/,
	 qq{Insert bogus '$bogus_item' bailiwick});
    $dbh->rollback;
  }
}

$dbh->do(q{INSERT INTO regano.bailiwicks (domain_tail) VALUES ('.test.')})
  if ($dbh->selectrow_array(q{SELECT COUNT(*) FROM regano.bailiwicks
				WHERE domain_tail = '.test.'}) == 0);
pass(q{Add '.test.' bailiwick});
eval {
  local $dbh->{PrintError};
  $dbh->do(q{INSERT INTO regano.bailiwicks (domain_tail) VALUES ('.test.')})
};
like($@, qr/duplicate key value violates unique constraint/,
     q{Verify unique '.test.' bailiwick});

{
  my $sth = $dbh->prepare(q{INSERT INTO regano.reserved_domains (domain_name, reason)
				VALUES (?, 'bogus')});
  $sth->{PrintError} = 0;
  foreach my $bogus_item ('example.test', 'example.test.',
			  '.example', '.example.', 'EXAMPLE') {
    $dbh->begin_work;
    eval { $sth->execute($bogus_item) };
    like($@, qr/violates check constraint/,
	 qq{Insert bogus '$bogus_item' reserved domain});
    $dbh->rollback;
  }
}

$dbh->do(q{INSERT INTO regano.reserved_domains (domain_name, reason)
		VALUES ('example', 'Reserved for testing purposes')})
  if ($dbh->selectrow_array(q{SELECT COUNT(*) FROM regano.reserved_domains
				WHERE domain_name = 'example'}) == 0);
pass(q{Reserve 'example' domain});
eval {
  local $dbh->{PrintError};
  $dbh->do(q{INSERT INTO regano.reserved_domains (domain_name, reason)
		VALUES ('example', 'bogus')})
};
like($@, qr/duplicate key value violates unique constraint/,
     q{Verify unique 'example' reserved domain});

##

sub make_salt ($) {
  $_[0] += 3 - $_[0] % 3 if $_[0] % 3; # round up to next multiple of 3
  my $salt = encode_base64(makerandom_octet( Length => $_[0], Strength => 0 ));
  chomp $salt;
  return $salt;
}

{
  my $reg_st = $dbh->prepare
    (q{SELECT regano_api.user_register(?, ROW(?,?,?), ?, ?)});
  my $chk_st = $dbh->prepare
    (q{SELECT COUNT(*) FROM regano.users WHERE username = ?});

  my $salt = make_salt 6;
  $reg_st->execute('admin',
		   'hmac_sha384/base64', $salt,
		   hmac_sha384_base64('password', $salt),
		   'Test System Admin',
		   'admin@example.test')
    unless $dbh->selectrow_array($chk_st, {}, 'admin');

  my $get_id = $dbh->prepare
    (q{SELECT id FROM regano.users WHERE username = ?});
  my $user_id = $dbh->selectrow_array($get_id, {}, 'admin');

  my $zone_template_id = $dbh->selectrow_array
    (q{INSERT INTO regano.domains
	(domain_name, domain_tail, owner_id, expiration)
	VALUES ('@', '.test.', ?, now())
	RETURNING id},
    {}, $user_id);
  my $hosted_template_id = $dbh->selectrow_array
    (q{INSERT INTO regano.domains
	(domain_name, domain_tail, owner_id, expiration)
	VALUES ('@@', '.test.', ?, now())
	RETURNING id},
    {}, $user_id);

  my $ins_NS = $dbh->prepare
    (q{INSERT INTO regano.domain_records
	(domain_id, seq_no, type, name, data_name)
	VALUES (?, regano.zone_next_seq_no(?), 'NS', '@', 'ns.test.glue.')});
  $dbh->do
    (q{INSERT INTO regano.domain_records
	(domain_id, seq_no, type, name, data_RR_SOA)
	VALUES (?, 0, 'SOA', '@',
		ROW('ns.test.glue.', 'hostmaster.test.test.',
			'3600', '1200', '7200', '900'))}, {}, $zone_template_id);
  $ins_NS->execute($zone_template_id, $zone_template_id);
  $ins_NS->execute($hosted_template_id, $hosted_template_id);
}

##

$dbh->disconnect;

__END__
