#!/usr/bin/env perl

=head1 NAME

regano-admin.pl - Regano administration tool

=head1 SYNOPSIS

  regano-admin.pl <VERB> <OBJECT> [<parameters>]

  VERB := { create | delete | reassign | ... }

  OBJECT := { account | bailiwick | domain | session }

  regano-admin.pl create account <username> <name> <email>
  regano-admin.pl recover account <username> [<contact id>]
  regano-admin.pl { delete | lock | recover | show } account <username>

  regano-admin.pl create bailiwick <name> <admin username> <zone master>
  regano-admin.pl reassign bailiwick <name> <admin username>
  regano-admin.pl delete bailiwick <name>

  regano-admin.pl { create | reassign } domain <name> <owner username>
  regano-admin.pl { delete | expire | preregister | renew } domain <name>

  regano-admin.pl delete session <username>
  regano-admin.pl reassign session <admin username> <username>

=head1 DESCRIPTION

Perform various administration tasks on a local Regano instance.  Some of
these tasks are included in regano-admin for convenience, while most are
impossible using the Web frontend for security reasons.


=head2 Account Management

=head3 regano-admin.pl create account <username> <name> <email>

Create account <username>.  The database requires every user account to
have a primary contact, which is created with <name> and <email>.  The
account is initially locked.

=head3 regano-admin.pl delete account <username>

Remove account <username> from the system.  The database will not permit an
account to be removed that currently holds any domains.

=head3 regano-admin.pl lock account <username>

Lock account <username> by setting the password to an invalid digest.

=head3 regano-admin.pl recover account <username> [<contact id>]

Begin the account recovery process by sending an email to the primary
contact address for account <username>.  A different contact may be chosen.

=head3 regano-admin.pl show account <username>

Print a summary of account <username>.


=head2 Baliwick Management

=head3 regano-admin.pl create bailiwick <name> <admin username> <zone master>
[zone-mirrors <mirror>...] [full-mirrors <mirror>...] [hostmaster <address>]
[refresh <seconds>] [retry <seconds>] [expire <seconds>] [minttl <seconds>]

Create bailiwick <name>, assign the internal template domains to the
account <admin username> and populate the template domains "@" and "@@" to
reflect the designated master and mirrors.  Mirrors in the zone-mirrors
list carry only the bailiwick zone itself, while mirrors in the
full-mirrors list also carry all hosted zones.  The hostmaster option
specifies an email address to put in the SOA record and defaults to
"hostmaster@<name>" in DNS encoding.  The refresh, retry, expire, and
minttl options provide values for the corresponding fields in the SOA
record for the bailiwick zone.

=head3 regano-admin.pl reassign bailiwick <name> <admin username>

Change the account that holds the template domains for bailiwick <name> to
account <admin username>.

=head3 regano-admin.pl delete bailiwick <name>

Remove bailiwick <name> from the system.  The database will not permit a
non-empty bailiwick to be removed.


=head2 Domain Management

=head3 regano-admin.pl create domain <name> <owner username>

Create domain <name> and assign it to account <owner username>.  This is
similar to the "register domain" option in the frontend except that this
command can create a reserved domain.

=head3 regano-admin.pl reassign domain <name> <owner username>

Change the account that holds domain <name> to account <owner username>.
This command will not operate on domains "@" or "@@".  To move those
domains, the bailiwick must be reassigned.

=head3 regano-admin.pl delete domain <name>

Immediately remove domain <name> from the system.  This command will not
remove the template domains used with a bailiwick.  To remove those
domains, the bailiwick must be deleted.

=head3 regano-admin.pl expire domain <name>

Set the expiration date for domain <name> to "now()" causing the domain to
immediately expire.  This is equivalent to the "release domain" option in
the frontend.

=head3 regano-admin.pl preregister domain <name>

Pre-register domain <name>.  This creates a record in the pending domains
table that can later be reassigned to an account.

=head3 regano-admin.pl renew domain <name>

Renew domain <name>.  This is equivalent to the "renew domain" option in
the frontend.


=head2 Session Management

=head3 regano-admin.pl delete session <username>

Remove all current seesions for account <username> from the system,
effectively logging <username> out.

=head3 regano-admin.pl reassign session <admin username> <username>

Change the account associated with a session belonging to account <admin
username> to the account <username> and set the session timestamps to
"now()".  There must be exactly one session associated with account <admin
username> when this command is issued.  That session will instead be
associated with account <username> as if account <username> had logged in.
This enables support to access user accounts.


=head1 AUTHOR

Pathore

=head1 LICENSE

This program is free software. You can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

use strict;
use warnings;

use Config::General;
use DBI;

our $VERB = shift @ARGV || '';
our $OBJECT = shift @ARGV || '';

our %config = (
  dsn           => 'dbi:Pg:db=regano',
  username      => '',
  password      => '',
  options       => { AutoCommit => 1, RaiseError => 1 },
);
{
    my $configfile;
    ($configfile = $0) =~ s~script/[^/]*$~regano-backend.conf~;
    $configfile = $ENV{REGANO_CONFIG} if $ENV{REGANO_CONFIG};
    my $reader = Config::General->new($configfile);
    my %contents = $reader->getall();
    my $confkey = (grep {m/^DB,/} keys %{$contents{Model}})[0];
    my $dbconf = $contents{Model}{$confkey};
    $config{$_} = $dbconf->{$_} for keys %$dbconf;
}

our $dbh = DBI->connect($config{dsn},
			$config{username},
			$config{password},
			$config{options});

if ($OBJECT eq 'account') {
    if ($VERB eq 'create' and scalar @ARGV >= 3) {
	my $username = shift @ARGV;
	my $name = shift @ARGV;
	$name .= ' '.(shift @ARGV) until $ARGV[0] =~ m/@/;
	my $email = shift @ARGV;

	$dbh->begin_work;
	my ( $user_id ) = $dbh->selectrow_array
	    (q[INSERT INTO regano.users (username, password, contact_id)
			VALUES (?, ROW('locked_account', '!', '!'), 1)
			RETURNING id],
	     {}, $username);
	$dbh->do
	    (q[INSERT INTO regano.contacts (owner_id, id, name, email)
			VALUES (?, 1, ?, ?)],
	     {}, $user_id, $name, $email);
	$dbh->commit;
    } elsif ($VERB eq 'delete' and scalar @ARGV == 1) {
	my $username = shift @ARGV;

	$dbh->do(q[DELETE FROM regano.users WHERE username = ?], {}, $username);
    } elsif ($VERB eq 'lock' and scalar @ARGV == 1) {
	my $username = shift @ARGV;

	$dbh->do(q[UPDATE regano.users
			SET password = ROW('locked_account', '!', '!')
			WHERE username = ?],
		 {}, $username);
    } elsif ($VERB eq 'recover' and scalar @ARGV == 1 || scalar @ARGV == 2) {
	my $username = shift @ARGV;
	my $contact_id = shift @ARGV;

	my ( $user_id, $primary_contact_id ) = $dbh->selectrow_array
	    (q[SELECT id, contact_id FROM regano.users WHERE username = ?],
	     {}, $username);
	$contact_id = $primary_contact_id unless defined $contact_id;
	$dbh->do(q[DELETE FROM regano.contact_verifications
			WHERE user_id = ?], {}, $user_id);
	$dbh->do(q[INSERT INTO regano.contact_verifications
			(id, key, type, user_id, contact_id)
			VALUES (gen_random_uuid(), gen_random_uuid(),
				'account_recovery', ?, ?)],
		 {}, $user_id, $contact_id );
	$dbh->do(q[NOTIFY regano__contact_verifications]);
    } elsif ($VERB eq 'show' and scalar @ARGV == 1) {
	my $username = shift @ARGV;

	my ( $user_id, $primary_contact_id,
	     $frontend_digest, $registered) = $dbh->selectrow_array
	    (q[SELECT id, contact_id, (password).xdigest, registered
			FROM regano.users WHERE username = ?],
	     {}, $username);

	print "Account $username: (id $user_id)\n";
	print "  using frontend password digest $frontend_digest\n";
	print "  registered at $registered\n";

	{
	    my ( $session_count, $oldest, $newest, $earliest, $latest) =
		$dbh->selectrow_array
		(q[SELECT COUNT(*),
			    MIN(start), MAX(start), MIN(activity), MAX(activity)
			FROM regano.sessions WHERE user_id = ?], {}, $user_id);
	    if ($session_count == 0) {
		print "  no sessions open\n";
	    } elsif ($session_count == 1) {
		print "  1 session open\n";
		print "    started $newest\n";
		print "    last activity $latest\n";
	    } else {
		print "  $session_count sessions open\n";
		print "    started ranging from $oldest\n";
		print "                      to $newest\n";
		print "    last activity ranging from $earliest\n";
		print "                            to $latest\n";
	    }
	}

	{
	    my ( $contact_count ) = $dbh->selectrow_array
		(q[SELECT COUNT(*) FROM regano.contacts WHERE owner_id = ?],
		 {}, $user_id);
	    if ($contact_count > 1) {
		print "  $contact_count contacts:\n";
	    } else {
		print "  $contact_count contact:\n";
	    }

	    my ( $id, $name, $email, $email_verified, $pgp_key_id );
	    my $sth = $dbh->prepare
		(q[SELECT id, name, email, email_verified, pgp_key_id
			FROM regano.contacts WHERE owner_id = ?
			ORDER BY id ASC]);
	    $sth->execute($user_id);
	    $sth->bind_columns(\ ( $id, $name, $email,
				   $email_verified, $pgp_key_id ));
	    while ($sth->fetch) {
		printf "  %2d: %s%s\n",
		$id, $name, ($id == $primary_contact_id ? "  [primary]" : "");
		print  "      $email";
		print "  [verified]" if $email_verified;
		print "\n";
		print  "      PGP Key ID: $pgp_key_id\n" if $pgp_key_id;
	    }
	}

	{
	    my ( $pending_domain, $pending_since ) = $dbh->selectrow_array
		(q[SELECT domain_name || domain_tail, start
			FROM regano.pending_domains WHERE user_id = ?],
		 {}, $user_id);

	    print "  domain $pending_domain pending since $pending_since\n"
		if defined $pending_domain;
	}

	{
	    my ( $domain_count ) = $dbh->selectrow_array
		(q[SELECT COUNT(*) FROM regano.domains WHERE owner_id = ?],
		 {}, $user_id);

	    if ($domain_count == 0) {
		print "  no domains registered\n";
	    } elsif ($domain_count == 1) {
		print "  1 domain registered\n";
	    } else {
		print "  $domain_count domains registered\n";
	    }

	    my ( $name, $registered, $expires, $last_update );
	    my $sth = $dbh->prepare
		(q[SELECT domain_name || domain_tail,
			    registered, expiration, last_update
			FROM regano.domains WHERE owner_id = ?]);
	    $sth->execute($user_id);
	    $sth->bind_columns(\ ( $name, $registered,
				   $expires, $last_update ));
	    while ($sth->fetch) {
		print "    domain $name\n";
		print "      registered $registered\n";
		print "      expires $expires\n";
		print "      last modified $last_update\n";
	    }
	}
    } else {
	print STDERR $0, ": unrecognized command\n";
    }
} elsif ($OBJECT eq 'bailiwick') {
    if ($VERB eq 'create' and scalar @ARGV >= 3) {
	my $bailiwick = shift @ARGV;
	my $admin_name = shift @ARGV;
	my $zone_master = shift @ARGV;
	$bailiwick =~ s/^/./ unless $bailiwick =~ m/^\./;
	$bailiwick =~ s/$/./ unless $bailiwick =~ m/\.$/;

	my @zone_mirrors = ($zone_master);
	my @full_mirrors = ($zone_master);
	my $SOA_hostmaster = 'hostmaster.opennic'.$bailiwick;
	my $SOA_refresh = 3600;
	my $SOA_retry = 1200;
	my $SOA_expire = 7200;
	my $SOA_minttl = 900;
	my $parse_state;

	while (scalar @ARGV) {
	    local $_ = shift @ARGV;
	    if (m/^(?:zone|full)-mirrors|hostmaster|refresh|retry|expire|minttl$/) {
		$parse_state = $_;
	    } elsif ($parse_state eq 'zone-mirrors') {
		push @zone_mirrors, $_;
	    } elsif ($parse_state eq 'full-mirrors') {
		push @full_mirrors, $_;
	    } elsif ($parse_state eq 'hostmaster') {
		$SOA_hostmaster = $_;
		$SOA_hostmaster =~ s/@/./;
	    } elsif ($parse_state eq 'refresh') {
		$SOA_refresh = $_;
	    } elsif ($parse_state eq 'retry') {
		$SOA_retry = $_;
	    } elsif ($parse_state eq 'expire') {
		$SOA_expire = $_;
	    } elsif ($parse_state eq 'minttl') {
		$SOA_minttl = $_;
	    }
	}

	$dbh->begin_work;
	my ( $admin_id ) = $dbh->selectrow_array
	    (q[SELECT id FROM regano.users WHERE username = ?],
	     {}, $admin_name);
	$dbh->do(q[INSERT INTO regano.bailiwicks (domain_tail) VALUES (?)],
		 {}, $bailiwick);
	my ( $zone_template_id ) = $dbh->selectrow_array
	    (q[INSERT INTO regano.domains
			(domain_name, domain_tail, owner_id, expiration)
			VALUES ('@', ?, ?, now())
			RETURNING id], {}, $bailiwick, $admin_id);
	my ( $hosted_template_id ) = $dbh->selectrow_array
	    (q[INSERT INTO regano.domains
			(domain_name, domain_tail, owner_id, expiration)
			VALUES ('@@', ?, ?, now())
			RETURNING id], {}, $bailiwick, $admin_id);

	$dbh->do(q[INSERT INTO regano.domain_records
			(domain_id, seq_no, type, name, data_RR_SOA)
			VALUES (?, 0, 'SOA', '@',
				ROW(?, ?, ?, ?, ?, ?))],
		 {}, $zone_template_id,
		 $zone_master, $SOA_hostmaster, $SOA_refresh,
		 $SOA_retry, $SOA_expire, $SOA_minttl);

	foreach my $server (@zone_mirrors) {
	    $dbh->do(q[INSERT INTO regano.domain_records
			(domain_id, seq_no, type, name, data_name)
			VALUES (?, regano.zone_next_seq_no(?), 'NS', '@', ?)],
		     {}, $zone_template_id, $zone_template_id, $server);
	}
	foreach my $server (@full_mirrors) {
	    $dbh->do(q[INSERT INTO regano.domain_records
			(domain_id, seq_no, type, name, data_name)
			VALUES (?, regano.zone_next_seq_no(?), 'NS', '@', ?)],
		     {}, $hosted_template_id, $hosted_template_id, $server);
	}

	$dbh->commit;
    } elsif ($VERB eq 'delete' and scalar @ARGV == 1) {
	my $bailiwick = shift @ARGV;
	$bailiwick =~ s/^/./ unless $bailiwick =~ m/^\./;
	$bailiwick =~ s/$/./ unless $bailiwick =~ m/\.$/;

	$dbh->begin_work;
	$dbh->do(q[DELETE FROM regano.domains
			WHERE domain_name IN ('@', '@@') AND domain_tail = ?],
		 {}, $bailiwick);
	$dbh->do(q[DELETE FROM regano.bailiwicks WHERE domain_tail = ?],
		 {}, $bailiwick);
	$dbh->commit;
    } elsif ($VERB eq 'reassign' and scalar @ARGV == 2) {
	my $bailiwick = shift @ARGV;
	my $new_admin = shift @ARGV;
	$bailiwick =~ s/^/./ unless $bailiwick =~ m/^\./;
	$bailiwick =~ s/$/./ unless $bailiwick =~ m/\.$/;

	$dbh->begin_work;
	my ( $new_admin_id ) = $dbh->selectrow_array
	    (q[SELECT id FROM regano.users WHERE username = ?], {}, $new_admin);
	$dbh->do(q[UPDATE regano.domains SET owner_id = ?
			WHERE domain_name IN ('@', '@@') AND domain_tail = ?],
		 {}, $new_admin_id, $bailiwick);
	$dbh->commit;
    } else {
	print STDERR $0, ": unrecognized command\n";
    }
} elsif ($OBJECT eq 'domain') {
    if ($VERB eq 'create' and scalar @ARGV == 2) {
	my $domain = shift @ARGV;
	my $username = shift @ARGV;
	my ( $domain_name, $domain_tail ) = ($domain =~ m/^([^.]+)([.].+)$/);

	$dbh->begin_work;
	my ( $user_id ) = $dbh->selectrow_array
	    (q[SELECT id FROM regano.users WHERE username = ?], {}, $username);
	my ( $expiration ) = $dbh->selectrow_array
	    (q[SELECT now() + (regano.config_get('domain/term')).interval]);
	$dbh->do(q[INSERT INTO regano.domains
			(domain_name, domain_tail, owner_id, expiration)
			VALUES (?, ?, ?, ?)],
		 {}, $domain_name, $domain_tail, $user_id, $expiration);
	$dbh->commit;
    } elsif ($VERB eq 'delete' and scalar @ARGV == 1) {
	my $domain = lc shift @ARGV;

	die "cannot delete template domains individually"
	    if $domain =~ m/^@+/;

	$dbh->do(q[DELETE FROM regano.domains
			WHERE lower(domain_name||domain_tail) = ?],{}, $domain);
	$dbh->do(q[DELETE FROM regano.pending_domains
			WHERE lower(domain_name||domain_tail) = ?],{}, $domain);
    } elsif ($VERB eq 'reassign' and scalar @ARGV == 2) {
	my $domain = lc shift @ARGV;
	my $username = shift @ARGV;

	die "cannot reassign template domains individually"
	    if $domain =~ m/^@+/;

	my ( $user_id ) = $dbh->selectrow_array
	    (q[SELECT id FROM regano.users WHERE username = ?], {}, $username);

	$dbh->do(q[UPDATE regano.domains SET owner_id = ?
			WHERE lower(domain_name||domain_tail) = ?],
		 {}, $user_id, $domain);
	$dbh->do(q[UPDATE regano.pending_domains SET owner_id = ?
			WHERE lower(domain_name||domain_tail) = ?],
		 {}, $user_id, $domain);
    } elsif ($VERB eq 'expire' and scalar @ARGV == 1) {
	my $domain = lc shift @ARGV;

	$dbh->do(q[UPDATE regano.domains SET expiration = now()
			WHERE lower(domain_name||domain_tail) = ?],
		 {}, $domain);
    } elsif ($VERB eq 'preregister' and scalar @ARGV == 1) {
	my $domain = lc shift @ARGV;
	my ( $domain_name, $domain_tail ) = ($domain =~ m/^([^.]+)([.].+)$/);

	$dbh->do(q[INSERT INTO regano.pending_domains
			(domain_name, domain_tail, user_id, start)
			VALUES (?, ?, NULL, NULL)],
		 {}, $domain_name, $domain_tail);
    } elsif ($VERB eq 'renew' and scalar @ARGV == 1) {
	my $domain = lc shift @ARGV;

	my ( $expiration ) = $dbh->selectrow_array
	    (q[SELECT now() + (regano.config_get('domain/term')).interval]);
	$dbh->do(q[UPDATE regano.domains SET expiration = ?
			WHERE lower(domain_name||domain_tail) = ?],
		 {}, $expiration, $domain);
    } else {
	print STDERR $0, ": unrecognized command\n";
    }
} elsif ($OBJECT eq 'session') {
    if ($VERB eq 'delete' and scalar @ARGV == 1) {
	my $username = shift @ARGV;

	my ( $user_id ) = $dbh->selectrow_array
	    (q[SELECT id FROM regano.users WHERE username = ?], {}, $username);

	$dbh->do(q[DELETE FROM regano.sessions WHERE user_id = ?],
		 {}, $user_id);
    } elsif ($VERB eq 'reassign' and scalar @ARGV == 2) {
	my $admin_username = shift @ARGV;
	my $username = shift @ARGV;

	my ( $admin_id ) = $dbh->selectrow_array
	    (q[SELECT id FROM regano.users WHERE username = ?],
	     {}, $admin_username);
	my ( $user_id ) = $dbh->selectrow_array
	    (q[SELECT id FROM regano.users WHERE username = ?], {}, $username);

	my ( $count ) = $dbh->selectrow_array
	    (q[SELECT COUNT(*) FROM regano.sessions WHERE user_id = ?],
	     {}, $admin_id);
	die "$admin_username must have exactly one session" unless $count == 1;
	$dbh->do(q[UPDATE regano.sessions
			SET user_id = ?, start = now(), activity = now()
			WHERE user_id = ?],
		 {}, $user_id, $admin_id);
    } else {
	print STDERR $0, ": unrecognized command\n";
    }
} else {
    print STDERR $0, ": unrecognized command\n";
}

1;
