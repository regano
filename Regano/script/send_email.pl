#!/usr/bin/env perl

=head1 NAME

send_email.pl - Regano contact verification email tool

=head1 SYNOPSIS

send_email.pl

=head1 DESCRIPTION

Monitor for new contact verification requests and send confirmation email

=head1 AUTHOR

Pathore and Verax

=head1 LICENSE

This program is free software. You can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

=head1 DEPENDENCIES

Requires Config::General, DBI, Net::SMTP (newer version that supports SSL),
and MIME::Entity from MIME-Tools package
You will also need to install DBD::Pg seperately

=cut

use strict;
use warnings;

use Config::General;
use DBI;

use Net::SMTP;
use MIME::Entity;
use Template;

################################################################################
## Global variables
################################################################################

=head1 CONFIGURATION

Write up of all configuration parameters that affect this script

=cut

our %config = (
  InstanceBase	=> 'http://localhost:3000',
  dsn		=> 'dbi:Pg:db=regano',
  username	=> '',
  password	=> '',
  options	=> { AutoCommit => 1, RaiseError => 1 },
  debug		=> 2
);
our %VTYPEMAP = (
  account_recovery => 'RecoverAccount',
  email_address => 'VerifyEmailAddress',
);
my $tt;

use constant SSL_VERIFY_NONE => Net::SSLeay::VERIFY_NONE();
use constant SSL_VERIFY_PEER => Net::SSLeay::VERIFY_PEER();
use constant SSL_VERIFY_FAIL_IF_NO_PEER_CERT => Net::SSLeay::VERIFY_FAIL_IF_NO_PEER_CERT();
use constant SSL_VERIFY_CLIENT_ONCE => Net::SSLeay::VERIFY_CLIENT_ONCE();

################################################################################
## Subroutines
################################################################################

# loads global %config hash
sub read_config () {
    my $basedir;
    ($basedir = $0) =~ s~script/[^/]*$~~;
    my $configfile = $basedir.'regano-backend.conf';
    $configfile = $ENV{REGANO_CONFIG} if $ENV{REGANO_CONFIG};
    my $reader = Config::General->new($configfile);
    my %contents = $reader->getall();
    my $confkey = (grep {m/^DB,/} keys %{$contents{Model}})[0];
    my $dbconf = $contents{Model}{$confkey};
    $config{$_} = $contents{$_} for grep {$contents{$_}}
    qw/InstanceBase Mail Message/;
    $config{$_} = $dbconf->{$_} for keys %$dbconf;

    #set up templates
    $tt = Template->new({INCLUDE_PATH => [$basedir.'/root/src']})
	or die "$Template::ERROR\n";
}

sub trace {
    shift @_ <= $config{debug} and print STDERR join(' ',@_),"\n";
}

sub send_verify ($$$$$) {
    my ($email, $name, $vid, $vkey, $vtype) = @_;
    my $encoding;
    my $message_body = '';
    my $message_config = $config{Message}{$VTYPEMAP{$vtype}};

    return unless $message_config;

    my $msg = MIME::Entity->build (
	Type		=> "multipart/mixed",
	#Boundary	=> "---1234567---",
	To		=> $email,
	From		=> $config{Mail}{SMTP}{Username}.'@'.$config{Mail}{SMTP}{Domain},
	Subject		=> $message_config->{Subject},
	'X-Mailer'	=> 'Regano', #TODO: add version number here
	);

    if ($message_config->{CharSet} eq 'UTF-8')
    {
	$encoding = '8bit';
    }
    else
    {
	print "WARN: Charset $message_config->{CharSet} is not supported, defaulting to US-ASCII\n"
	    if ($message_config->{CharSet} ne 'US-ASCII');
	$encoding = '7bit';
	$message_config->{CharSet} = 'US-ASCII';
	#TODO: whine if 8-bit chars are found in any strings
    }

    #build message from template
    $tt->process($message_config->{Template_text},
		 { email_address => $email,
		   InstanceBase => $config{InstanceBase},
		   token => "$vid/$vkey" },
		 \$message_body);
    trace 1, "Output:\n".$message_body."EOT\n";

    $msg->attach(
	Type		=> 'text/plain',
	Encoding	=> $encoding,
	Charset		=> $message_config->{CharSet},
	#Data		=> join("\n", $config{mail_top}, "$config{InstanceBase}/verify/$vid/$vkey", $config{mail_bottom})
	Data		=> $message_body
	);

    #TODO: Optional attachments
    #TODO: Sign/encrypt with GPG

    trace 1, "Sending message:\n", $msg->as_string;

    #TODO: SMTPS - this may or may not work
    my $use_ssl = ( $config{Mail}{SMTP}{UseTLS} eq 'yes' ) ? 1 : 0;

    #Start the connection
    trace 2, "Attempting connection to $config{Mail}{SMTP}{Host}:$config{Mail}{SMTP}{Port} from $config{Mail}{SMTP}{Domain}, SSL = $use_ssl";
    my $smtp = Net::SMTP->new(
	Hello		=> $config{Mail}{SMTP}{Domain},
	Host		=> $config{Mail}{SMTP}{Host},
	Port		=> $config{Mail}{SMTP}{Port},
	SSL		=> $use_ssl,
	Debug		=> 1 #HACK
	);

    $IO::Socket::SSL::DEBUG = 1; #HACK

    unless ( defined $smtp )
    {
	print "Error connecting to mail server: $@\n";
	return 5;
    }


    if ( $config{Mail}{SMTP}{UseTLS} eq 'start' )
    {
	unless ( $smtp->starttls( SSL_verify_mode => SSL_VERIFY_NONE ) ) #TODO: Set from config (no sensible way yet)
	{
	    print "$IO::Socket::SSL::SSL_ERROR \n";
	    return 3;
	}
    }

    #TODO: Test SASL
    if ( $config{Mail}{SMTP}{Password} ne '' )
    {
	unless ( $smtp->auth($config{Mail}{SMTP}{Username}, $config{Mail}{SMTP}{Password}) )
	{
	    #TODO: Find error message
	    return 4;
	}
    }

    #start email
    $smtp->mail($config{Mail}{SMTP}{Username});
    if ( $smtp->recipient($email) )
    {
	$smtp->data($msg->as_string . ".\r\n");
    }
    else
    {
	#Error with recipient
	print "Error: ", $smtp->message();
	$smtp->quit();
	return 2;
    }
    $smtp->quit();
    $msg->purge(); #clean any asociated files.

    return 1;
}

sub process_verifications ($) {
    my $dbh = shift;
    my $count = 0;
    my $list_st = $dbh->prepare_cached
	(q{SELECT v.id, v.key, v.type, c.email, c.name
	       FROM regano.contact_verifications v
		   JOIN regano.contacts c
		       ON v.user_id = c.owner_id AND v.contact_id = c.id
	       WHERE NOT v.email_sent});
    my $update_st = $dbh->prepare_cached
	(q{UPDATE regano.contact_verifications
	       SET email_sent = TRUE
	       WHERE id = ? AND key = ?});
    my ($vid, $vkey, $vtype, $email, $name);

    $dbh->begin_work;

    $list_st->execute;
    $list_st->bind_columns(\ ($vid, $vkey, $vtype, $email, $name));

    while ($list_st->fetch) {
	$update_st->execute($vid, $vkey)
	    if send_verify($email, $name, $vid, $vkey, $vtype);
	$count++;
    }

    $dbh->commit;

    return $count;
}

################################################################################
## Signal handlers
################################################################################

=head1 SIGNALS

=cut

################################################################################
## Main event loop
################################################################################

read_config();

# adapted from DBD::Pg manual
 DBHLOOP: {
     my $dbh = DBI->connect($config{dsn},
			    $config{username},
			    $config{password},
			    $config{options});
     $dbh->ping() or die "bad DB handle";

     $dbh->do('LISTEN regano__contact_verifications');
     process_verifications($dbh);

   NOTIFYLOOP: {
       while (my $notify = $dbh->pg_notifies) {
	   my ($eventname, $pid, $payload) = @$notify;
	   if ($eventname eq 'regano__contact_verifications') {
	       process_verifications($dbh);
	   }
       }

       { # adapted from example in perlfunc
	   my ($rin, $win, $ein, $rout, $wout, $eout, $bits);
	   $bits = ''; vec($bits, $dbh->{pg_socket},1) = 1;
	   $rin = $ein = $bits; $win = '';
	   select($rout=$rin, $wout=$win, $eout=$ein, undef);
       }
       $dbh->ping() or redo DBHLOOP;
       redo NOTIFYLOOP;
     }
}

1;
