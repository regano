package Regano::Model::DB::Zone;
use Moose;
use namespace::autoclean;

extends 'Regano::Model::DB';

=head1 NAME

Regano::Model::DB::Zone - Catalyst Model

=head1 DESCRIPTION

Database bridge for DNS zone records

=encoding utf8

=head1 METHODS

=cut


=head2 get_last_update

Return last-update timestamp for the named domain in Unix time.

=cut

sub get_last_update {
    my ( $self, $zone_name ) = @_;
    my $dbh = $self->dbh;

    my $get_timestamp_st = $dbh->prepare_cached
	(q{SELECT EXTRACT(EPOCH FROM
			  date_trunc('second',
				     regano_api.domain_last_update(?)))});

    my ($timestamp) = $dbh->selectrow_array
	($get_timestamp_st, {}, $zone_name);

    return $timestamp;
}


=head2 domains_in_bailiwick

Return three arrayrefs.

The first value returned is an array of domains that are INLINE in the
given bailiwick.  The second value returned is an array of domains that are
HOSTED in the given bailiwick.  The third value returned is an array of
domains that are DELEGATED from the given bailiwick.

=cut

sub domains_in_bailiwick {
    my ( $self, $zone_name ) = @_;
    my $dbh = $self->dbh;

    my @inline = ();
    my @hosted = ();
    my @delegated = ();

    my $get_bailiwick_st = $dbh->prepare_cached
	(q{SELECT domain_tail
		FROM regano.bailiwicks
		WHERE lower(domain_tail) = lower(?)});
    my $get_domains_st = $dbh->prepare_cached
	(q{SELECT domain_name, regano_api.domain_mode(domain_name||domain_tail)
		FROM regano.domains
		WHERE domain_tail = ? AND now() < expiration});

    my ( $domain_tail ) = $dbh->selectrow_array
	($get_bailiwick_st, {}, $zone_name);

    return [], [], [] unless defined $domain_tail;

    {
	$get_domains_st->execute($domain_tail);
	my ( $name, $mode );
	$get_domains_st->bind_columns(\ ($name, $mode));

	while ($get_domains_st->fetch) {
	    if ($name =~ m/^@/) {
		next; # skip internal zones
	    } elsif ($mode eq 'INLINE') {
		push @inline, $name;
	    } elsif ($mode eq 'HOSTED') {
		push @hosted, $name;
	    } elsif ($mode eq 'DELEGATED') {
		push @delegated, $name;
	    } else {
		die "unknown domain mode $mode for $name in $domain_tail";
	    }
	}
    }

    return \@inline, \@hosted, \@delegated;
}


=head2 records_for_domain

Return arrayref of hashes representing records for the named domain and a
hashref of metadata for the domain itself.

Each hash contains name/class/type/ttl, and a data key that holds a hash
with the fields and values for this record.

The second value returned contains the default TTL and zone serial number.

=cut

sub records_for_domain {
    my ( $self, $zone_name ) = @_;
    my @records = ();
    my %types = ();
    my $dbh = $self->dbh;

    my $get_domain_info_st = $dbh->prepare_cached
	(q{SELECT id, EXTRACT(EPOCH FROM default_ttl),
		    EXTRACT(EPOCH FROM date_trunc('second', last_update))
		FROM regano.domains
		WHERE lower(domain_name||domain_tail) = lower(?)});
    my $get_records_st = $dbh->prepare_cached
	(q{SELECT seq_no, name, class, type, EXTRACT(EPOCH FROM ttl),
		  data_name, data_text, data_RR_A, data_RR_AAAA,
		  (data_RR_SOA).master, (data_RR_SOA).mbox,
		  EXTRACT(EPOCH FROM (data_RR_SOA).refresh),
		  EXTRACT(EPOCH FROM (data_RR_SOA).retry),
		  EXTRACT(EPOCH FROM (data_RR_SOA).expire),
		  EXTRACT(EPOCH FROM (data_RR_SOA).minimum),
		  (data_RR_DS).*,
		  (data_RR_MX).*,
		  (data_RR_SRV).*
		FROM regano.domain_records
		WHERE domain_id = ?});

    my ( $domain_id, $default_ttl, $serial ) = $dbh->selectrow_array
	($get_domain_info_st, {}, $zone_name);

    return [], {} unless defined $domain_id;

    {
	$get_records_st->execute($domain_id);
	my ( $seq_no, $name, $class, $type, $ttl,
	     $data_name, $data_text, $data_A, $data_AAAA,
	     # SOA
	     $SOA_master, $SOA_mbox, $SOA_refresh,
	     $SOA_retry, $SOA_expire, $SOA_minimum,
	     # DS
	     $DS_key_tag, $DS_algorithm, $DS_digest_type, $DS_digest,
	     # MX
	     $MX_preference, $MX_exchange,
	     # SRV
	     $SRV_priority, $SRV_weight, $SRV_port, $SRV_target );
	$get_records_st->bind_columns
	    (\ ($seq_no, $name, $class, $type, $ttl,
		$data_name, $data_text, $data_A, $data_AAAA,
		# SOA
		$SOA_master, $SOA_mbox, $SOA_refresh,
		$SOA_retry, $SOA_expire, $SOA_minimum,
		# DS
		$DS_key_tag, $DS_algorithm, $DS_digest_type, $DS_digest,
		# MX
		$MX_preference, $MX_exchange,
		# SRV
		$SRV_priority, $SRV_weight, $SRV_port, $SRV_target));
	while ($get_records_st->fetch) {
	    $types{$type}++;
	    $records[$seq_no] = { name => $name,
				  class => $class,
				  seq_no => $seq_no,
				  type => $type,
				  ttl => $ttl };
	    if (($type eq 'CNAME')||($type eq 'DNAME')
		||($type eq 'NS') ||($type eq 'PTR')) {
		$records[$seq_no]->{data} = { name => $data_name }
	    } elsif (($type eq 'SPF')||($type eq 'TXT')) {
		$records[$seq_no]->{data} = { text => $data_text }
	    } elsif ($type eq 'A') {
		$records[$seq_no]->{data} = { address => $data_A }
	    } elsif ($type eq 'AAAA') {
		$records[$seq_no]->{data} = { address => $data_AAAA }
	    } elsif ($type eq 'DS') {
		$records[$seq_no]->{data} = { key_tag => $DS_key_tag,
					      algorithm => $DS_algorithm,
					      digest_type => $DS_digest_type,
					      digest => $DS_digest }
	    } elsif ($type eq 'MX') {
		$records[$seq_no]->{data} = { preference => $MX_preference,
					      exchange => $MX_exchange }
	    } elsif ($type eq 'SRV') {
		$records[$seq_no]->{data} = { priority => $SRV_priority,
					      weight => $SRV_weight,
					      port => $SRV_port,
					      target => $SRV_target }
	    } elsif ($type eq 'SOA') {
		$records[$seq_no]->{data} = { master => $SOA_master,
					      mbox => $SOA_mbox,
					      serial => $serial,
					      refresh => $SOA_refresh,
					      retry => $SOA_retry,
					      expire => $SOA_expire,
					      minimum => $SOA_minimum }
	    } else {
		die "unknown DNS record type"
	    }
	}
    }

    return \@records, {default_ttl => $default_ttl,
		       serial => $serial};
}

=head1 AUTHOR

Pathore

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
