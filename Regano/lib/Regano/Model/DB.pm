package Regano::Model::DB;

use strict;
use warnings;
use parent 'Catalyst::Model::DBI';

__PACKAGE__->config(
  dsn           => 'dbi:Pg:db=regano',
  user          => '',
  password      => '',
  options       => { AutoCommit => 1, RaiseError => 1 },
);

=head1 NAME

Regano::Model::DB - DBI Model Class

=head1 SYNOPSIS

See L<Regano>

=head1 DESCRIPTION

This is the database glue module for Regano.

=head1 METHODS

=head2 bailiwick_tails

Get a reference to an array of known domain tails.

=cut

sub bailiwick_tails {
    my $self = shift;
    my $dbh = $self->dbh;
    my $sth = $dbh->prepare_cached
	(q[SELECT domain_tail FROM regano.bailiwicks]);
    $sth->execute;
    my (@bailiwicks, $tail);
    $sth->bind_columns(\$tail);
    push @bailiwicks, $tail while $sth->fetch;

    return \@bailiwicks;
}

=head2 domain_info

Get an information record for a domain.

=cut

sub domain_info {
    my $self = shift;
    my $domain_name = shift;
    my $dbh = $self->dbh;

    my $domain_info_st = $dbh->prepare_cached
	(q[SELECT id, domain_name||domain_tail AS name,
		   regano_api.domain_status(domain_name||domain_tail) AS status,
		   registered, expiration, last_update,
		   default_ttl, owner_id
	       FROM regano.domains
	       WHERE lower(domain_name||domain_tail) = lower(?)]);
    $domain_info_st->execute($domain_name);
    my $row = $domain_info_st->fetchrow_hashref('NAME_lc');
    1 while $domain_info_st->fetch;

    return $row;
}

=head2 domains_for_master

Get a reference to an array of domains served by a particular master server.

=cut

sub domains_for_master {
    my $self = shift;
    my $master_name = shift;
    my $dbh = $self->dbh;

    my $domain_search_st = $dbh->prepare_cached
	(q[SELECT domain_name||domain_tail AS name
		FROM regano.domains JOIN regano.domain_records
		    ON (id = domain_id)
		WHERE type = 'SOA'
		    AND (data_RR_SOA).master = ?]);
    $domain_search_st->execute($master_name);
    my (@domains, $name);
    $domain_search_st->bind_columns(\$name);
    push @domains, $name while $domain_search_st->fetch;

    s/^@+\.// for @domains;

    return \@domains;
}

=head2 master_for_domain

Get the master server for the bailiwick containing a domain.

=cut

sub master_for_domain {
    my $self = shift;
    my $domain_name = shift;
    my $dbh = $self->dbh;

    return undef unless $domain_name =~ m/^[^.]*(\..*\.)$/;

    my $lookup_st = $dbh->prepare_cached
	(q[SELECT (data_RR_SOA).master
		FROM regano.domains d INNER JOIN regano.domain_records rr
		    ON (d.id = rr.domain_id)
		WHERE d.domain_name = '@' AND d.domain_tail = ?
		    AND rr.type = 'SOA']);

    $lookup_st->execute($1);
    my $master = $lookup_st->fetchrow_array;
    1 while $lookup_st->fetch;

    return $master;
}

=head1 AUTHOR

Pathore

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
