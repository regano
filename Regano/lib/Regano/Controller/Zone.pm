package Regano::Controller::Zone;
use Moose;
use namespace::autoclean;

use HTTP::Date;

BEGIN { extends 'Catalyst::Controller'; }

has 'MinimumTTL' => ( is => 'ro', isa => 'Int' );

__PACKAGE__->config(
    MinimumTTL => '100'
);

=head1 NAME

Regano::Controller::Zone - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Regano::Controller::Zone in Zone.');
}

=head2 zone

Return DNS records for requested zone.

=cut

sub zone :Path :Args(1) {
    my ( $self, $c, $zone_name ) = @_;

    return $c->response->redirect($c->request->uri . '.')
	unless $zone_name =~ m/\.$/;

    my $timestamp = $c->model('DB::Zone')->get_last_update($zone_name);
    $c->response->headers->header( Last_Modified => time2str($timestamp) );

    my $timestamp_conditional =
	$c->request->headers->header('If-Modified-Since');
    if (defined $timestamp_conditional) {
	my $client_timestamp = str2time($timestamp_conditional);
	return $c->response->status(304)
	    if (defined $client_timestamp
		&& $client_timestamp >= $timestamp);
    }

    # TODO: process this according to RFC2068
    my $format = $c->request->headers->header('Accept');
    if (defined $format && $format =~ m{text/dns}) {
	$format = 'zone';
	$c->stash( current_view => 'Raw' );
	$c->response->content_type('text/dns');
    } else {
	$format = 'html';
    }

    my $type = $c->model('DB::API')->domain_status($zone_name);

    $c->response->headers->push_header( Vary => 'Accept' );

    if ($type eq 'REGISTERED') {
	# return records for single domain
	my ($records, $zone_info) =
	    $c->model('DB::Zone')->records_for_domain($zone_name);
	shift @$records unless exists $records->[0];
	$c->stash( template => 'zone/domain_'.$format.'.tt',
		   MinimumTTL => $self->MinimumTTL,
		   zone => { name => $zone_name,
			     default_ttl => $zone_info->{default_ttl},
			     records => $records } );
    } elsif ($type eq 'BAILIWICK') {
	# return bailiwick zone

	return $c->response->redirect
	    ($c->uri_for_action('/zone/zone').'/.'.$zone_name)
	    unless $zone_name =~ m/^\./;

	my %records = ();	# domain name -> record set
	my %info = ();		# domain name -> info block
	my ($inline, $hosted, $delegated) =
	    $c->model('DB::Zone')->domains_in_bailiwick($zone_name);
	my $zone_name_dns = $zone_name;
	$zone_name_dns =~ s/^\.//;

	foreach my $domain (qw/@ @@/, @$inline, @$delegated) {
	    ($records{$domain}, $info{$domain}) =
		$c->model('DB::Zone')->records_for_domain($domain.$zone_name);
	    shift @{$records{$domain}} unless exists $records{$domain}->[0];
	}

	my @records = ();	# flattened record list

	# load TLD zone records
	foreach my $rec (@{$records{'@'}}) {
	    $rec->{name} = '@';
	    push @records, $rec;
	}
	# load records for inline and delegated zones
	foreach my $domain (@$inline) {
	    foreach my $rec (@{$records{$domain}}) {
		if ($rec->{name} eq '@') {
		    $rec->{name} = $domain;
		} else {
		    $rec->{name} .= '.'.$domain;
		}
		push @records, $rec;
	    }
	}
	# load records for hosted zones
	foreach my $domain (@$hosted) {
	    foreach my $rec (@{$records{'@@'}}) {
		$rec->{name} = $domain;
		push @records, $rec;
	    }
	}
	# load records for delegated zones
	foreach my $domain (@$delegated) {
	    foreach my $rec (@{$records{$domain}}) {
		if ($rec->{name} eq '@') {
		    $rec->{name} = $domain;
		} else {
		    $rec->{name} .= '.'.$domain;
		}
		$rec->{data}{name} .= '.'.$domain.$zone_name
		    if $rec->{type} eq 'NS' and $rec->{data}{name} !~ m/\.$/;
		push @records, $rec;
	    }
	}

	$c->stash( template => 'zone/domain_'.$format.'.tt',
		   MinimumTTL => $self->MinimumTTL,
		   zone => { name => $zone_name_dns,
			     default_ttl => $info{'@'}->{default_ttl},
			     records => \@records } );
    } else {
	$c->response->status(404);
	$c->stash( template => 'zone/not_found.tt',
		   zone => { name => $zone_name },
		   current_view => 'HTML' );
    }
}



=encoding utf8

=head1 AUTHOR

Pathore

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
