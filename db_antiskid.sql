-- Regano database DoS protection module
--
-- Uses PostgreSQL extensions.
--
--  Regano is a domain registration system for OpenNIC TLDs written in
--  Perl.  This file is part of Regano.
--
--  Regano may be distributed under the same terms as Perl itself.  Of
--  particular importance, note that while regano is distributed in the
--  hope that it will be useful, there is NO WARRANTY OF ANY KIND
--  WHATSOEVER WHETHER EXPLICIT OR IMPLIED.

-- The type definitions in db_types.sql must already be installed.
-- The function definitions in db_functions.sql must already be installed.
-- The main API definitions in db_api.sql must already be installed.
-- The configuration in db_config.sql must be loaded for these to actually work.

-- Currently blocked clients
CREATE TABLE IF NOT EXISTS regano.antiskid_block (
	client		inet PRIMARY KEY,
	blocked		timestamp with time zone
				NOT NULL DEFAULT CURRENT_TIMESTAMP
) WITH (fillfactor = 90);
CREATE INDEX ON regano.antiskid_block (blocked);

-- Tally of expensive operations
CREATE TABLE IF NOT EXISTS regano.antiskid_tally (
	client		inet NOT NULL,
	points		integer NOT NULL
				CHECK(points > 0),
	tallied		timestamp with time zone
				NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(client, tallied, points)
);
CREATE INDEX ON regano.antiskid_tally (tallied);

ALTER TABLE regano.antiskid_block OWNER TO regano;
ALTER TABLE regano.antiskid_tally OWNER TO regano;

-- Check if a client may perform an expensive operation.
CREATE OR REPLACE FUNCTION regano_api.antiskid_check
	(op text, addr inet)
	RETURNS boolean AS $$
DECLARE
    block_expire	CONSTANT interval NOT NULL
			    := (regano.config_get('antiskid/block')).interval;
    points_expire	CONSTANT interval NOT NULL
			    := (regano.config_get('antiskid/tally')).interval;
    points_threshold	CONSTANT integer NOT NULL
			    := (regano.config_get('antiskid/tally')).number;

    add_points		integer NOT NULL
			    := (regano.config_get('antiskid/weight/'||op)).number;
    current_points	integer;

    block		regano.antiskid_block%ROWTYPE;
BEGIN
    -- clean up blocked client list and check if client is blocked
    DELETE
	FROM regano.antiskid_block
	WHERE blocked < (now() - block_expire);
    SELECT * INTO block
	FROM regano.antiskid_block
	WHERE client = addr;
    IF FOUND THEN
	IF block.blocked < (now() - (block_expire / 2)) THEN
	    UPDATE regano.antiskid_block
		SET blocked = now()
		WHERE client = addr;
	END IF;
	RETURN FALSE;
    END IF;

    -- tally the new points and check if client should be blocked
    INSERT INTO regano.antiskid_tally (client, points)
	VALUES (addr, add_points);
    SELECT SUM(points) INTO STRICT current_points
	FROM regano.antiskid_tally
	WHERE client = addr AND tallied >= (now() - points_expire);
    IF current_points > points_threshold THEN
	INSERT INTO regano.antiskid_block (client) VALUES (addr);
	RETURN FALSE;
    END IF;

    -- clean up point tallies and return success
    DELETE
	FROM regano.antiskid_tally
	WHERE tallied < (now() - points_expire);
    RETURN TRUE;
END;
$$ LANGUAGE plpgsql VOLATILE STRICT SECURITY DEFINER;
ALTER FUNCTION regano_api.antiskid_check (text, inet)
	OWNER TO regano;

-- Check for valid session from non-blocked client
CREATE OR REPLACE FUNCTION regano_api.antiskid_session_check
	(id uuid, addr inet)
	RETURNS text AS $$
DECLARE
    block_expire	CONSTANT interval NOT NULL
			    := (regano.config_get('antiskid/block')).interval;
BEGIN
    PERFORM *
	FROM regano.antiskid_block
	WHERE client = addr AND blocked > (now() - block_expire);
    IF FOUND THEN
	RETURN NULL;
    ELSE
	RETURN regano_api.session_check(id);
    END IF;
END;
$$ LANGUAGE plpgsql VOLATILE STRICT SECURITY DEFINER;
ALTER FUNCTION regano_api.antiskid_session_check (uuid, inet)
	OWNER TO regano;
