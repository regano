#!/usr/bin/perl

# Database intialization for Regano

#  Regano is free software.  You can redistribute it and/or modify
#  it under the same terms as Perl itself.

use DBI;
use strict;
use warnings;

my $dbh;

$dbh = DBI->connect('dbi:Pg:db=template1', undef, undef,
		    {AutoCommit => 1, RaiseError => 1})
  or die $DBI::errstr;

eval {
  local $dbh->{PrintError};
  print "ensure 'regano' user exists\n";
  $dbh->do(q[CREATE ROLE "regano" WITH LOGIN]);
};
eval {
  local $dbh->{PrintError};
  print "ensure 'regano-www' user exists\n";
  $dbh->do(q[CREATE ROLE "regano-www" WITH LOGIN]);
};

eval {
  local $dbh->{PrintError};
  $dbh->do(q[DROP DATABASE "regano"]);
  print "erase database 'regano'\n";
} if defined $ARGV[0] and $ARGV[0] eq 'erasedb';
$dbh->do(q[CREATE DATABASE "regano" WITH OWNER = "regano"]);
print "create database 'regano'\n";

$dbh->disconnect;

foreach my $part (qw(types tables permissions functions config api antiskid)) {
  system {'psql'} qw(psql -d regano -f), "db_${part}.sql"
}

$dbh = DBI->connect('dbi:Pg:db=regano', undef, undef,
		    {AutoCommit => 1, RaiseError => 1})
  or die $DBI::errstr;

print "reserve \"@\" domain for storing TLD records\n";
$dbh->do
(q[INSERT INTO regano.reserved_domains (domain_name, reason)
	VALUES ('@', 'Reserved for storing TLD records')]);

print "reserve \"@@\" domain for storing NS records for hosted zones\n";
$dbh->do
(q[INSERT INTO regano.reserved_domains (domain_name, reason)
	VALUES ('@@', 'Reserved for storing NS records for host mirrors')]);

print "reserve \"example\" domain\n";
$dbh->do
(q[INSERT INTO regano.reserved_domains (domain_name, reason)
	VALUES ('example', 'Reserved for use in documentation')]);

print "reserve IANA names:";
foreach (qw/rfc-editor gtld-servers iana-servers root-servers
	    aso dnso pso iab iana icann iesg ietf irtf istf
	    internic afrinic arin apnic lacnic latnic ripe/) {
  print ' '.$_;
  $dbh->do
    (q[INSERT INTO regano.reserved_domains (domain_name, reason)
	VALUES (?, 'Reserved to avoid confusion with IANA')], {}, $_);
}
print "\n";

print "reserve OpenNIC names:";
foreach (qw/register registrar opennic openic nic dot/) {
  print ' '.$_;
  $dbh->do
    (q[INSERT INTO regano.reserved_domains (domain_name, reason)
	VALUES (?, 'Reserved for OpenNIC')], {}, $_);
}
print "\n";

$dbh->disconnect;

__END__
