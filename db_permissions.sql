-- Regano database permissions
--
-- Uses PostgreSQL extensions.
--
--  Regano is a domain registration system for OpenNIC TLDs written in
--  Perl.  This file is part of Regano.
--
--  Regano may be distributed under the same terms as Perl itself.  Of
--  particular importance, note that while regano is distributed in the
--  hope that it will be useful, there is NO WARRANTY OF ANY KIND
--  WHATSOEVER WHETHER EXPLICIT OR IMPLIED.

-- The type definitions in db_types.sql must already be installed.
-- The table definitions in db_tables.sql must already be installed.

GRANT USAGE ON SCHEMA regano_api TO "regano-www";
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA regano_api TO "regano-www";

GRANT USAGE ON SCHEMA regano TO "regano-www";

GRANT SELECT ON TABLE regano.bailiwicks		TO "regano-www";
GRANT SELECT ON TABLE regano.domains		TO "regano-www";
GRANT SELECT ON TABLE regano.domain_records	TO "regano-www";